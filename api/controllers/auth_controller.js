const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Models
const User = require("../../models/User.js");

// Express Validator
const { validationResult } = require("express-validator/check");


// Config
const config = require("../../config/config.js");



// Check If User Exists
const checkIfUserExists = (req, res, next) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const customError = new Error("Incorrect data: Sign Up");
    customError.status = 400;
    return next(customError);
  }

  User.find({
    email: req.body.email
  }).then( (users) => {
    if (users.length > 0) {
      const customError = new Error("Email is already used");
      customError.status = 400;
      return next(customError);
    } else {
      return next();
    }
    return next();
  }).catch( (error) => {
    console.log(error)
    const customError = new Error("Something went wrong during sign up");
    customError.status = 400;
    return next(customError);
  })
}


// Sign Up New User
const signUp = (req, res, next) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const customError = new Error("Incorrect data: Sign Up");
    customError.status = 400;
    return next(customError);
  }

  bcrypt.hash(req.body.password, 10, (err, hash) => {
    if (err) {
      const err = new Error("User could not be created");
      err.status = 400;
      return next(err);
    } else {
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        email: req.body.email,
        username: req.body.username,
        password: hash
      });

      user.save().then( (result) => {
        console.log(`Result ${result}`);

        res.status(200).json({
          message: "New user has been created"
        })
      }).catch( error => {
        const customError = new Error("User has not been created");
        customError.status = 400;
        return next(customError);
      })
    }
  })
}


const signIn = (req, res, next) => {
  console.log(req.body)
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const customError = new Error("Incorrect data: Sign In");
    customError.status = 400;
    return next(customError);
  }

  User.findOne({
    email: req.body.email
  })
  .exec()
  .then( (user) => {
      
    console.log(`User: ${user}`)

    if (!user) {
      const customError = new Error("Incorrect login or password");
      customError.status = 400;
      return next(customError);
    }

    bcrypt.compare(req.body.password, user.password, (err, result) => {
      
      if (err) {
        const customError = new Error("Something went wrong during sign in");
        customError.status = 400;
        return next(customError);
      } 

      if (result) {
        // return res.status(200).json({
          // success: "Sign In - Success"
        // })

        const JWTToken = jwt.sign({
          email: user.email,
          _id: user._id
        },
          config.secret,
          {
            expiresIn: "2h"
          });
          return res.status(200).json({
            message: "Sign In - Success",
            token: JWTToken
          })
      }

      const customError = new Error("Unauthorized Access");
      customError.status = 401;
      return next(customError);




    })
  });
}





module.exports = {
  checkIfUserExists,
  signUp,
  signIn
}