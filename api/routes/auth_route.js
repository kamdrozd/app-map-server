const express = require("express");

// Express Validator
const { check } = require("express-validator/check");

const router = express.Router();




const authController = require("../controllers/auth_controller.js");

router.post("/signup", [
  check("email").exists(),
  check("username").exists(),
  check("password").exists(),
  check("password").isLength({ min: 6 }),
  check("email").isEmail()
],  
authController.checkIfUserExists,
authController.signUp);

router.post("/signin", [
  check("email").exists(),
  check("password").exists(),
  check("password").isLength({ min: 6 }),
  check("email").isEmail()
],  
authController.signIn);


module.exports = router;