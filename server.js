const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const config = require("./config/config.js");

mongoose.connect(config.database);


// Testing Endpoint
app.get("/", (req, res) => {
  res.status(200).json({
    "message": "Hello World",
  });
});


// routes

const authRoutes = require("./api/routes/auth_route.js");
app.use("/", authRoutes)


// Error Handling
app.use(function (err, req, res, next) {
  if (!err.status) {
    err.status = 404;
  }
  res.status(err.status).json({
    "message": err.message
  });
})

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});