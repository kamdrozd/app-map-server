const mongoose = require("mongoose");


const UserSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
});


module.exports = mongoose.model("User", UserSchema);